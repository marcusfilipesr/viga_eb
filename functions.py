import numpy as np
from scipy.linalg import eigh

def solveProblem(KK,MM,L,L_ef,n_gdl):
    U, D = eigh(KK,MM)

    v = np.arange(0,len(D[:,0]),n_gdl)

    Lo = np.arange(0,L,L_ef)

    modo = D[v.astype(int),:]
    modo = modo/modo[-1]

    return modo, Lo