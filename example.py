class Dog:
    """Creates a dog object
    """
    especie = "canis-lupus-familiaris"

    def __init__(self,name,age,raca):
        self.name = name
        self.age = age
        self.raca = raca
    
    def latir(self):
        print("Wooof!!!")
    
    def pular(self):
        print(f"{self.name} acabou de pular!")
if __name__ == "__main__":

    dog1 = Dog("João Cleber","2","Golden")
    
    dog1.pular()
